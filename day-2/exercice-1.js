
export const my_alpha_reverse = (str) => {

	if (typeof str === "string") {
		let i = 0
		let revStr = ""
		while (str[i] !== undefined) {
			revStr = str[i] + revStr
			i++
		}

		return revStr
	}
}

// console.log(my_alpha_reverse("Bonjour"))
