
export const my_display_unicode = (arr) => {

	let i = 0
	let sentence = ""
	while (arr[i] !== undefined) {

		if ((arr[i] >= 97 && arr[i] <= 122) || (arr[i] >= 65 && arr[i] <= 90)
			|| (arr[i] >= 49 && arr[i] <= 57) || arr[i] === 32) {
			let value = "" + arr[i]
			sentence += String.fromCharCode(value)
			i++
		}
	}

	return sentence
}

// let decimals = [66, 111, 110, 106, 111, 117, 114, 32, 116, 111, 117, 116, 32, 108, 101, 32, 109, 111, 110, 100, 101]

// console.log(my_display_unicode(decimals))
