
export const my_display_alpha = () => {
	
	let start_alpha = 'a'
	let alphas = ""
	for (let i = 0; i < 26; i++) {
		let current = start_alpha.charCodeAt(0) + i
		alphas += String.fromCharCode(current)
	}
	
	return alphas
}

