
export const my_is_posi_neg = (nbr) => {
	
	if (typeof nbr === undefined || nbr === null) {
		return "POSITIF"
	}
	
	if (typeof nbr === "number") {
		if (nbr === 0) {
			return "NEUTRAL"
		}
		if (nbr > 0) {
			return "POSITIF"
		}
		if (nbr < 0) {
			return "NEGATIF"
		}
	}

}

