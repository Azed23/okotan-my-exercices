import {my_display_alpha} from "./exercice-1.js";


export const my_display_alpha_reverse = () => {
	
	let alph = my_display_alpha()
	let alpha_reverse = ""
	for (let i = 25; i >= 0; i--) {
		alpha_reverse += alph[i]
	}

	return alpha_reverse
}

